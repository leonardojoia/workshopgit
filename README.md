# WorkShopGit

1.  Escolher um editor de sua preferência para resolver problemas futuros! (instalar Notepad++, visual studio code)
2.  Baixar e instalar o Git - `https://git-scm.com/download/win`
3.  Baixar e instalar o PPK - `https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html`
4.  Abrir o puttygen
5.  Gerar uma chaves
6.  salvar ela em uma pasta com nome ".ssh" dentro do seu usuário do windows 
7.  Baixar e instalar o SourceTree - `https://www.sourcetreeapp.com`(será preciso criar uma conta no bitbucket)
8.  Carregar a chave no sourcetree
9.  Adicionar a chave publica a sua conta no Gitlab
9.  verificar se a chave funciona tambem no gitbash
10. Verificar se a chave funciona no sourcetree

